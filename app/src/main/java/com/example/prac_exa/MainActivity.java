package com.example.prac_exa;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtTrabajador;
    private Button btnEntrar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        txtTrabajador = findViewById(R.id.txtTrabajador);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void ingresar() {
        String trabajador = "juan";
        if (trabajador.equals(txtTrabajador.getText().toString())) {
            Bundle bundle = new Bundle();
            bundle.putString("trabajador", txtTrabajador.getText().toString());

            Intent intent = new Intent(MainActivity.this, ReciboActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "No autorizado.", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Inicio");
        confirmar.setMessage("¿Quieres salir?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {
            // No hacer nada
        });
    }
}