package com.example.prac_exa;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboActivity extends AppCompatActivity {
    private TextView lblTrabajador;
    private EditText txtNumRec;
    private EditText txtNombre;
    private EditText txtHTrabajadas;
    private EditText txtHExtras;
    private RadioGroup gruporbtn;
    private RadioButton rbtnAux;
    private RadioButton rbtnAlba;
    private RadioButton rbtnIng;
    private EditText txtSubTotal;
    private EditText txtImpuesto;
    private EditText txtTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private int puestoSelect;
    private ReciboNomina reciboNomina = new ReciboNomina(0, 0, 0.0, 0.0, 0.0, "");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarInfo();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresarInicio();
            }
        });

        Bundle datos = getIntent().getExtras();
        String trabajador = datos.getString("trabajador");
        lblTrabajador.setText(trabajador);
    }

    private void iniciarComponentes() {
        lblTrabajador = findViewById(R.id.lblTrabajador);
        txtNumRec = findViewById(R.id.txtNumRec);
        txtNombre = findViewById(R.id.txtNombre);
        txtHTrabajadas = findViewById(R.id.txtHTrabajadas);
        txtHExtras = findViewById(R.id.txtHExtras);
        gruporbtn = findViewById(R.id.gruporbtn);
        rbtnAux = findViewById(R.id.rbtnAux);
        rbtnAux.setChecked(true);
        rbtnAlba = findViewById(R.id.rbtnAlba);
        rbtnIng = findViewById(R.id.rbtnIng);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtTotal = findViewById(R.id.txtTotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

    }

    private void calcularNomina() {
        puestoSelect = gruporbtn.getCheckedRadioButtonId();

        if (txtHTrabajadas.getText().toString().isEmpty() ||
                txtHExtras.getText().toString().isEmpty() ||
                txtNombre.getText().toString().isEmpty() ||
                txtNumRec.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Ingrese todos los datos para calcular.",
                    Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (puestoSelect == rbtnAux.getId()) {
                reciboNomina.setPuesto(1);
                puestoSelect = 0;
            } else if (puestoSelect == rbtnAlba.getId()) {
                reciboNomina.setPuesto(2);
                puestoSelect = 0;
            } else if (puestoSelect == rbtnIng.getId()) {
                reciboNomina.setPuesto(3);
                puestoSelect = 0;
            }

            reciboNomina.setHorasTNormal(Double.valueOf(txtHTrabajadas.getText().toString()));
            reciboNomina.setHorasTExtras(Double.valueOf(txtHExtras.getText().toString()));

            double totalAP = reciboNomina.calcularTotal();
            txtTotal.setText(String.valueOf(totalAP));
            txtSubTotal.setText(String.valueOf(reciboNomina.calcularSubtotal()));
            txtImpuesto.setText(String.valueOf(reciboNomina.calcularImpuesto()));
        }
    }

    private void limpiarInfo() {
        txtNumRec.setText("");
        txtNombre.setText("");
        txtHTrabajadas.setText("");
        txtHExtras.setText("");
        txtImpuesto.setText("");
        txtSubTotal.setText("");
        txtTotal.setText("");
        rbtnAux.setChecked(false);
        rbtnAlba.setChecked(false);
        rbtnIng.setChecked(false);
        puestoSelect = 0;
    }

    private void regresarInicio() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculador de nómina");
        confirmar.setMessage("¿Regresar al MainActivity?");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {
            // No hacer nada
        });
        confirmar.show();
    }
}
