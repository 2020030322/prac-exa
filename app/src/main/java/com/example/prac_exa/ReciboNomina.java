package com.example.prac_exa;

public class ReciboNomina {
    private int numRecibo;
    private int puesto;
    private Double horasTNormal;
    private Double horasTExtras;
    private Double impuestoPorc;
    private String nombre;

    public ReciboNomina(ReciboNomina RN){
        this.numRecibo = RN.numRecibo;
        this.puesto = RN.puesto;
        this.horasTNormal = RN.horasTNormal;
        this.horasTExtras = RN.horasTExtras;
        this.impuestoPorc = RN.impuestoPorc;
        this.nombre = RN.nombre;
    }
    public ReciboNomina(int numRecibo, int puesto, Double horasTNormal, Double horasTExtras, Double impuestoPorc, String nombre){
        this.numRecibo = numRecibo;
        this.puesto = puesto;
        this.horasTNormal = horasTNormal;
        this.horasTExtras = horasTExtras;
        this.impuestoPorc = impuestoPorc;
        this.nombre = nombre;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public Double getHorasTrabNormal() {
        return horasTNormal;
    }

    public void setHorasTNormal(Double horasTNormal) { this.horasTNormal = horasTNormal; }

    public Double getHorasTExtras() {
        return horasTExtras;
    }

    public void setHorasTExtras(Double horasTExtras) { this.horasTExtras = horasTExtras; }

    public Double getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(Double impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double calcularSubtotal(){
        Double subtotal = 0.0;
        switch (puesto){
            case 1: subtotal = ((240*horasTNormal)+(480*horasTExtras)); break;
            case 2: subtotal = ((300*horasTNormal)+(600*horasTExtras)); break;
            case 3: subtotal = ((400*horasTNormal)+(800*horasTExtras)); break;
        }
        return subtotal;
    }
    public Double calcularImpuesto(){
        Double calcImp = 0.0;
        calcImp = calcularSubtotal()*.16;
        return calcImp;
    }
    public Double calcularTotal(){
        return (calcularSubtotal() - calcularImpuesto());
    }
}

